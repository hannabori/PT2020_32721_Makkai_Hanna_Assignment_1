package model;

import Model.Monom;
import Model.Polinom;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PolinomCalculatorTest {
    private static Polinom polinom;
    private static Polinom polinom2;

    @BeforeClass
    // This annotation will ensure that the setup method is run once, before running all tests
    public static void setup() {
        polinom = new Polinom();
        polinom2 = new Polinom();
    }

    @Test
    public void addZeroTest() {
        // First, do the test setup
        polinom.addMonom(new Monom(0, 1));
        polinom2.addMonom(new Monom(5, 1));
        Polinom result = polinom2.plus(polinom);

        Polinom expectedResult = new Polinom();
        expectedResult.addMonom(new Monom(5,1));

        // Verify result against expected value
        assertEquals(expectedResult, result);
    }

    @Test
    public void plusTest() {
        polinom.addMonom(new Monom(3, 1));
        polinom2.addMonom(new Monom(2, 1));

        Polinom expectedResult = new Polinom();
        expectedResult.addMonom(new Monom(5, 1));

        Polinom result = polinom.plus(polinom2);

        assertEquals(expectedResult, result);
    }

    @Test
    public void minusTest() {
        polinom.addMonom(new Monom(3, 1));
        polinom2.addMonom(new Monom(2, 2));

        Polinom expectedResult = new Polinom();
        expectedResult.addMonom(new Monom(3, 1));
        expectedResult.addMonom(new Monom(-2, 2));

        Polinom result = polinom.minus(polinom2);

        assertEquals(expectedResult, result);
    }

    @Test
    public void oriTest() {
        polinom.addMonom(new Monom(1, 1));
        polinom.addMonom(new Monom(-1, 0));
        polinom2.addMonom(new Monom(1, 1));
        polinom2.addMonom(new Monom(-1, 0));

        Polinom expectedResult = new Polinom();
        expectedResult.addMonom(new Monom(1, 0));
        expectedResult.addMonom(new Monom(-2, 1));
        expectedResult.addMonom(new Monom(1, 2));
        expectedResult.sort();

        Polinom result = polinom.ori(polinom2);

        assertEquals(expectedResult, result);
    }

    @Test
    public void derivatTest() {
        Polinom p1 = new Polinom();
        p1.addMonom(new Monom(4, 3));
        //p1.setGrad(3);
        Polinom expectedResult = new Polinom();
        expectedResult.addMonom(new Monom(12, 2));
        //expectedResult.setGrad(2);

        Polinom result = p1.derivat();

        assertEquals(expectedResult, result);
    }

    @Test
    public void integratTest() {
        Polinom p = new Polinom();
        p.addMonom(new Monom(8, 3));

        Polinom expectedResult = new Polinom();
        expectedResult.addMonom(new Monom(2, 4));

        Polinom result = p.integrat();

        assertEquals(expectedResult, result);

    }

}
