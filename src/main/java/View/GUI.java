package View;

import Controller.Controller;
import Model.Monom;
import Model.Polinom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.awt.Color.*;

public class GUI extends JFrame {

    JPanel p; //panelul care contine elementele urmatoare de graphic user interface (GUI), chenarul principal
    JLabel l1;
    public JLabel l2;
    public JTextArea t1 = new JTextArea(); //spatiu pentru introducerea primului polinom
    public JTextArea t2 = new JTextArea(); //spatiu pentru a introducere al doilea polinom
    public JTextArea t3 = new JTextArea(); //spatiu rezervat afisarii rezultatului
    Controller controller = new Controller(this); //Controllerul creat primeste ca parametru GUI-ul actual
    public final JButton b_sub = new JButton("−");
    public final JButton b_add = new JButton("+");
    public final JButton b_mult = new JButton("×");
    public final JButton b_div = new JButton("÷");
    public final JButton b_deriv = new JButton("( )'");
    public final JButton b_integr = new JButton("∫( )");

    //constructor care creeaza GUI-ul apeland metoda de initializare "init"
    public GUI() {
        init();
    }

    //metoda de initializare a GUI-ului, care construieste elment cu element GUI-ul
    void init() {
        setSize(510, 260);
        setTitle("Calculator de polinoame");

        p = new JPanel();//panelul continand toate elementele din GUI
        p.setBackground(lightGray);//culoarea fundalului
        p.setForeground(black); // culoarea scrisului

        //eticheta cu rol de ghidare
        l1 = new JLabel("Introdu polinoamele si alege operatia dorita: ");
        l1.setBounds(20, 10, 300, 20);
        /*
        * dimensiunile chenarului etichetei l1:
        * (
        * x = distanta de la marginea stanga,
        * y = distanta de a marginea de sus,
        * width = latimea etichetei,
        * height = inaltimea etichetei
        * )
        */
        l1.setBackground(lightGray);
        l1.setForeground(black);
        //l1.setOpaque(true);
        p.add(l1); //adaugarea etichetei la panel

        //eticheta pentru afisarea mesajului de eroare
        l2 = new JLabel("");
        //initial mesajul de eroare e null, in caz ca apare o eroare textul etichetei se va schimba
        l2.setBounds(20, 170, 400, 30);
        l2.setFont(l2.getFont().deriveFont(16f));
        l2.setBackground(lightGray);
        l2.setForeground(new Color(204,0,0));
        p.add(l2);

        //campul unde se poate introduce primul polinom
        t1.setBounds(20, 40, 250, 30);
        t1.setEditable(true);
        t1.setBackground(darkGray);
        t1.setForeground(white);
        t1.setOpaque(true);
        p.add(t1);

        //campul unde se poate introduce al doilea polinom
        t2.setBounds(20, 80, 250, 30);
        t2.setEditable(true);
        t2.setBackground(darkGray);
        t2.setForeground(white);
        t2.setOpaque(true);
        p.add(t2);

        //campul rezervat rezultatului
        t3.setBounds(20, 130, 250, 30);
        t3.setEditable(false);
        t3.setBackground(darkGray);
        t3.setForeground(white);
        t3.setOpaque(true);
        p.add(t3);

        //butonul care executa adunarea polinoamelor
        b_add.setBounds(290, 40, 55, 30);
        b_add.setFont(b_add.getFont().deriveFont(18f));
        b_add.setBackground(darkGray);
        b_add.setForeground(cyan);
        p.add(b_add);
        b_add.addActionListener(controller);

        //butonul care executa scaderea polinoamelor
        b_sub.setBounds(290, 80, 55, 30);
        b_sub.setFont(b_sub.getFont().deriveFont(18f));
        b_sub.setBackground(darkGray);
        b_sub.setForeground(cyan);
        p.add(b_sub);
        b_sub.addActionListener(controller);

        //butonul care executa inmultirea polinoamelor
        b_mult.setBounds(355, 40, 55, 30);
        b_mult.setFont(b_mult.getFont().deriveFont(18f));
        b_mult.setBackground(darkGray);
        b_mult.setForeground(cyan);
        p.add(b_mult);
        b_mult.addActionListener(controller);

        //butonul care NU executa impartirea polinoamelor
        b_div.setBounds(355, 80, 55, 30);
        b_div.setFont(b_div.getFont().deriveFont(18f));
        b_div.setBackground(darkGray);
        b_div.setForeground(cyan);
        p.add(b_div);
        b_div.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                l2.setText("Haha, buton cu scop estetic.");
            }
        });

        //butonul care executa derivarea polinoamelor
        b_deriv.setBounds(420, 40, 55, 30);
        b_deriv.setBackground(darkGray);
        b_deriv.setForeground(cyan);
        p.add(b_deriv);
        b_deriv.addActionListener(controller);

        //butonul care executa integrarea polinoamelor
        b_integr.setBounds(420, 80, 55, 30);
        b_integr.setBackground(darkGray);
        b_integr.setForeground(cyan);
        p.add(b_integr);
        b_integr.addActionListener(controller);

        //eticheta pentru a marca locul rezultatului
        final JLabel b_eq = new JLabel("=");
        b_eq.setBounds(290, 130, 55, 30);
        b_eq.setFont(b_eq.getFont().deriveFont(25f));
        b_eq.setBackground(lightGray);
        b_eq.setForeground(darkGray);
        b_eq.setOpaque(true);
        p.add(b_eq);

        p.setLayout(null); //salveaza setarile grafice punand layoutul initial la null
        p.setOpaque(true);
        this.add(p); //adaugarea panelului de GUI la containerul principal
        this.setDefaultCloseOperation(EXIT_ON_CLOSE); //fereastra de GUI se inchide la apasarea butonului X din colt
        this.setVisible(true); //seteaza vizibilitatea GUI-ului la 100%
    }
}
