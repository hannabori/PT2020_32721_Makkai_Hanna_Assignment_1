package Model;

import java.util.Objects;

public class Monom {

    public double coef; //coeficient
    public double putere; //puterea la care se afla monomul

    //metoda folosita pentru a returna coeficientul monomului
    public double getCoef() {
        return coef;
    }

    //metoda folosita pentru a returna puterea monomului
    public double getPutere() {
        return putere;
    }

    //metoda folosita pentru a seta coeficientul monomului la valoarea data
    public void setCoef(double coef) {
        this.coef = coef;
    }

    //metoda folosita pentru a seta puterea monomului la valoarea data
    public void setPutere(double putere) {
        this.putere = putere;
    }

    //constructor care creeaza un monom nou, setand coeficientul si puterea lui din valori date
    public Monom(double coef, double putere) {
        this.coef = coef;
        this.putere = putere;
    }

    public Monom(){}

    @Override
    public String toString() {

        String MonomCuSemn = "";

        if (coef == 0);
        else if (putere == 0) {
            MonomCuSemn += String.format(("%.0f"), coef);
        } else if (putere == 1 && coef == 1) {
            MonomCuSemn = "x";
        } else if (putere == 1 && coef ==-1) {
            MonomCuSemn = "-x";
        } else if (putere !=0 && putere !=1 && coef ==1){
            MonomCuSemn = String.format(("x^%.0f"), putere);
        } else if (putere !=0 && putere !=1 && coef ==-1){
            MonomCuSemn = String.format(("-x^%.0f"), putere);
        } else if (putere !=0 && (coef != 1 && coef != -1 )) {
            MonomCuSemn = String.format(("%.0fx^%.0f"), coef, putere);
        } else if (putere ==1 && (coef != 1 && coef != -1 && coef !=0)){
            MonomCuSemn = String.format(("%.0fx"), coef);
        }else
            System.out.println("Format error!");

         return MonomCuSemn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Monom monom = (Monom) o;
        return Double.compare(monom.coef, coef) == 0 &&
                Double.compare(monom.putere, putere) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coef, putere);
    }
}
