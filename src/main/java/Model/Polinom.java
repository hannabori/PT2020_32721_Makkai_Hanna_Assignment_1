package Model;

import Controller.Comparator;

import java.util.*;

public class Polinom {

    private int grad;
    private List<Monom> monoms;

    public Polinom(int grad) {
        this.grad = grad;
        this.monoms = new ArrayList<Monom>(grad);
    }

    public Polinom() {
        this.grad = 0;
        this.monoms = new ArrayList<Monom>();
    }

    public Polinom(Polinom p) {
        monoms = new ArrayList<Monom>();
        this.grad = p.getGrad();
        for(Monom m : p.getMonoms()) {
            monoms.add(new Monom(m.getCoef(), m.getPutere()));
        }
    }

    public int getGrad() {
        return grad;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

    public List<Monom> getMonoms() {
        return monoms;
    }

    public void setMonoms(List<Monom> monoms) {
        this.monoms = monoms;
    }

    public void addMonom(Monom monom) {
        boolean monomExists = false;
        for(Monom m : monoms) {
            if(m.getPutere() == monom.getPutere()) {
                double coef = m.getCoef();
                if (coef + monom.getCoef() != 0) {
                    m.setCoef(coef + monom.getCoef());
                } else {
                    monoms.remove(m);
                }
                monomExists = true;
                break;
            }
        }
        if (!monomExists) {
            this.monoms.add(monom);
            if (this.grad < monom.getPutere()) {
                setGrad((int) monom.getPutere());
            }
        }
    }

    public Monom getMonom(int index) {
        Monom monom = new Monom();
        if (index < this.monoms.size() && this.monoms.get(index) != null) {
            monom = this.monoms.get(index);
        }
            return monom;
    }

    public void setMonom(int index, int coef) {
        this.monoms.get(index).setCoef(coef);
    }

    @Override
    public String toString() {
            String pol= "";
            for (Monom m : this.monoms) {
                if (m.getCoef()>0){
                    pol += " + "+ m.toString();
                } else {
                    pol += " " + m.toString();
                }
            }
            return pol;
        }

   @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Polinom polinom1 = (Polinom) o;
        boolean b = grad == polinom1.grad;
        boolean b2 = monoms.equals(polinom1.monoms);
        return b&&b2;
    }

    @Override
    public int hashCode() {
        return Objects.hash(grad, monoms);
    }

    public Polinom plus(Polinom polinom) {
        Polinom result = new Polinom(this);
        for (Monom m : polinom.getMonoms()) {
            result.addMonom(m);
        }
        return result;
    }

    public Polinom minus(Polinom polinom) {

        Polinom result = new Polinom(this);
        for (Monom m : polinom.getMonoms()) {
            double coef = m.getCoef();
            m.setCoef((-1)*coef);
            result.addMonom(m);
        }
        return result;
    }

    public Polinom ori(Polinom polinom) {
        int size = this.getGrad() + polinom.getGrad();
        Polinom result = new Polinom(size);

        for (Monom m : this.getMonoms()) {
            for (Monom n : polinom.getMonoms()) {
                double coef = m.getCoef() * n.getCoef();
                double putere = m.getPutere() + n.getPutere();
                result.addMonom(new Monom(coef, putere));
            }
        }

        /*for (int i=0; i <= size; i++) {
            result.addMonom(new Monom(0, i));
        }

        for (int i = 0; i <= this.getGrad(); i++ ) {
            for (int j = 0; j <= polinom.getGrad(); j++) {

                Monom monom = result.getMonom(i+j);
                int sum = (int) (monom.getCoef() + this.getMonom(i).getCoef() * polinom.getMonom(j).getCoef());
                result.setMonom(i+j, sum);
            }
        }*/
        result.sort();
        return result;
    }

    public void sort() {
        Collections.sort(monoms, new Comparator());
    }

    public Polinom derivat() {
        Polinom result = new Polinom(this.getGrad() - 1);

        for(Monom monom: this.getMonoms()) {
            if (monom.getPutere() != 0) {
                result.addMonom(new Monom(monom.getCoef() * monom.getPutere(), monom.getPutere() - 1));
            }
        }

        return result;
    }

    public Polinom integrat() {
        Polinom result = new Polinom(this.getGrad() + 1);
        for(Monom monom : this.getMonoms()) {
            result.addMonom(new Monom(monom.getCoef() / (monom.getPutere() + 1), monom.getPutere() + 1));
        }
        return result;
    }
}
