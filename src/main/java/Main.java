import Model.Monom;
import Model.Polinom;
import View.GUI;

public class Main {

    public static void main(String[] args){

         new GUI();

        //DEMO-1
        /*
        Polinom p1 = new Polinom(2);

        p1.addMonom(new Monom(1, 0));
        p1.addMonom(new Monom(2, 1));
        p1.addMonom(new Monom(1, 2));

        Polinom p2= new Polinom(3);

        p2.addMonom(new Monom(6, 0));
        p2.addMonom(new Monom(2, 1));
        p2.addMonom(new Monom(3, 2));
        p2.addMonom(new Monom(4, 3));

        System.out.println(p1);
        System.out.println(p2);

        System.out.println();
        System.out.println("p1 + p2 = " + p1.plus(p2));

        System.out.println();
        System.out.println("p1 - p2 = " + p1.minus(p2));

        System.out.println();
        System.out.println("(p1)' = " + p1.derivat());

        System.out.println();
        System.out.println(" ∫(p1) = " + p1.integrat() + " + C");

        System.out.println();
        System.out.println("p1*p2 = " + 	p1.ori(p2));
        */

        //DEMO-2
        /*
        Polinom p = new Polinom();
        Polinom p2 = new Polinom(); //
            boolean run = true;
            while (run) {

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter first monom coefficient: ");
                try {
                    double coef = sc.nextDouble();

                    System.out.println("Enter first monom power: ");
                    double power = sc.nextDouble();

                    Monom monom = new Monom(coef, power);
                    p.adaugareMonom(monom); //adaugam monomul creat la polinomul nou
                    System.out.println("You have entered the monom: " + monom.toString());

                } catch (Exception e) {
                    System.out.println("The character introduced wasn't a number!");
                }

                System.out.println("Would you like to introduce another monom? Y/N");
                boolean no = true;
                while (no) {
                    char c = sc.next().charAt(0);

                    if (c == 'Y' || c == 'y') {
                        no = false;
                    }  else if (c == 'N' || c == 'n') {
                        System.out.println("The polynomial you've created is: " + p.toString());
                        System.out.println("Enter another polynomial!");

                        boolean t = true;
                        while (t) {
                            System.out.println("Enter monom coefficient: ");
                            try {
                                double coe = sc.nextDouble();

                                System.out.println("Enter monom power: ");
                                double pow = sc.nextDouble();

                                Monom monom1 = new Monom(coe, pow);
                                p2.adaugareMonom(monom1); //adaugam monomul creat la polinomul nou
                                System.out.println("You have entered the monom: " + monom1.toString());

                            } catch (Exception e) {
                                System.out.println("The character introduced wasn't a number!");
                            }

                            System.out.println("Would you like to introduce another monom? Y/N");
                            boolean nop = true;
                            while (nop) {
                                char ch = sc.next().charAt(0);

                                if (ch == 'Y' || ch == 'y') {
                                    nop = false;
                                } else if (ch == 'N' || ch == 'n') {
                                    System.out.println("The polynomial you've created is: " + p2.toString());

                                    //Polinom suma = p.plus(p2);
                                    Polinom dif = p.minus(p2);
                                    Polinom p1deriv = p.derivare();
                                    //Polinom p2deriv = p2.derivare();
                                    //System.out.println("Suma polinoamelor e p1+p2 = " + suma.toString());
                                    System.out.println("Diferenta intre polinoame p1-p2 = " + dif.toString());
                                    System.out.println("Derivatul polinomului p1 = " + p1deriv.toString());
                                    //System.out.println("Derivatul polinomului p2 = " + p2deriv.toString());
                                    run = false;
                                    t = false;
                                    nop = false;
                                } else {
                                    System.out.println("Please type y or n!");
                                }
                            }
                        }

                        //run = false;
                        no = false;
                    } else {
                        System.out.println("Please type y or n!");
                        }
                }
        }*/
    }
}
