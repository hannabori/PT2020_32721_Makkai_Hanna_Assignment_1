package Controller;

        import Model.Monom;

public class Comparator implements java.util.Comparator<Monom> {
    @Override
    public int compare(Monom m, Monom n) {
        if (m.getPutere() < n.getPutere()) {
            return 1;
        }

        if (m.getPutere() > n.getPutere()) {
            return -1;
        }
        return 0;
    }
}
