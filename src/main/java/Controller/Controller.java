package Controller;

import Model.Monom;
import Model.Polinom;
import View.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller implements ActionListener {

    private GUI gui;

    public Controller(GUI gui){
        this.gui = gui;
    }

    public void actionPerformed(ActionEvent e)  {
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();
        Polinom result;

        try {
            Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
            final String polinomNou1 = gui.t1.getText(); //salveaza Stringul introdus in GUI
            final String polinomNou2 = gui.t2.getText(); //salveaza Stringul introdus in GUI

            Matcher mat1 = p.matcher(polinomNou1);
            while (mat1.find()) {
                double p1Coef = Double.parseDouble(mat1.group(1));
                double p1Degree = Double.parseDouble(mat1.group(2));
                p1.addMonom(new Monom(p1Coef, p1Degree));
                System.out.println("Coef: " + mat1.group(1));
                System.out.println("Degree: " + mat1.group(2));
            }

            Matcher mat2 = p.matcher( polinomNou2);
            while (mat2.find()) {
                double p2Coef = Double.parseDouble(mat2.group(1));
                double p2Degree = Double.parseDouble(mat2.group(2));
                p2.addMonom(new Monom(p2Coef, p2Degree));
                System.out.println("Coef: " + mat2.group(1));
                System.out.println("Degree: " + mat2.group(2));
            }
            if(e.getSource() == gui.b_add) {
                result = p1.plus(p2);
                gui.t3.setText(result.toString());
            } else if (e.getSource() == gui.b_sub){
                result = p1.minus(p2);
                gui.t3.setText(result.toString());
            } else if (e.getSource() == gui.b_mult){
                result = p1.ori(p2);
                gui.t3.setText(result.toString());
            } else if (e.getSource() == gui.b_deriv){
                result = p1.derivat();
                gui.t3.setText(result.toString());
            } else if (e.getSource() == gui.b_integr){
                result = p1.integrat();
                gui.t3.setText(result.toString() + " + C");
            }

            gui.t3.setVisible(true);
            gui.t3.setOpaque(true);
            gui.l2.setText("");

        } catch (Exception ex) {
        gui.l2.setText("Intrare nevalida! Va rugam introduceti un polinom!");
        gui.l2.setOpaque(true);
        }
    }
}
